extends Label

onready var timer = MGFramework.get_timer()

# Called when the node enters the scene tree for the first time.
#func _ready():
#	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	updateText();

func updateText():
	if timer.time_left > 0.06:
		set_text("%.2f" % timer.time_left)
	else:
		set_text("Time's Up!")
