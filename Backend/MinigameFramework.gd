# S&T GDC Minigame collection framework
# Created by Athakaspen 
# First version 2020-09-15
extends Node



# Playsession Statistics
var totalScore := 0;
var games_played := 0;
var games_won := 0;

var gameScore := 0;

# Timer
onready var timer := Timer.new();

const WINTEXT = preload("res://Backend/WinText/YouWin.tscn")
const LOSETEXT = preload("res://Backend/WinText/YouLose.tscn")
const SCOREPOPUP = preload("res://Backend/Widgets/ScorePopup.tscn")

const MainMenu = "res://Main Menu/MainMenu.tscn"
const LevelBase = "res://Backend/LevelBaseScene/LevelBase.tscn"
const LevelPath = {
	"Game 1": "res://Minigames/_TestGame1/Game_1.tscn",
	"Game 2": "res://Minigames/_TestGame2/Game_2.tscn",
	"Sort": "res://Minigames/Sort/Sort.tscn",
	"Maze"  : "res://Minigames/MazeGame/Maze.tscn",
	"Meteors": "res://Minigames/Meteors/Meteors.tscn",
	"Guard": "res://Minigames/Guard/Guard.tscn",
	"Balloon": "res://Minigames/Balloon/Balloon.tscn"
}

func _ready():
	timer.one_shot = true;
	timer.autostart = false;
	add_child(timer)
	randomize()

func add_points(points:int) -> void:
	gameScore += points

func set_score(score:int) -> void:
	gameScore = score;

# Used to start a new game
func start_game(game : String) -> void:
	gameScore = 0; # ensure game score starts at 0
# warning-ignore:return_value_discarded
	get_tree().change_scene(LevelPath[game]);

# Creates an animated score popup at the specified location
func spawnScorePopup(x:float, y:float, score:int):
	# Format score as a string
	var scoreText = "";
	if !(score < 0): scoreText += "+"
	scoreText += str(score)
	
	# instance popup
	var popup = SCOREPOPUP.instance();
	popup.position = Vector2(x,y);
	popup.get_node("Container/Label").set_text(scoreText);
	get_tree().get_current_scene().add_child(popup);


# Each game calls this fucntion when it finishes
func end_game(winState:bool) -> void:
	
	# increment stat counters
	games_played += 1 
	if winState==true:
		games_won += 1
	
	# add score
	totalScore += gameScore
	
	# Wintext splash
	get_tree().paused = true	# pause game
	displayWinText(winState)
	yield(get_tree().create_timer(1.6), "timeout");
	get_tree().paused = false	# unpause
	gameScore = 0 # reset score before next game
	
	#return to main menu
# warning-ignore:return_value_discarded
	get_tree().change_scene(MainMenu);

# Displays "You win!" or "You lose!" over current scene
func displayWinText(winState) -> void:
	if winState == true:
		get_tree().get_current_scene().add_child(WINTEXT.instance())
	else:
		get_tree().get_current_scene().add_child(LOSETEXT.instance())

# return the timer
func get_timer() -> Node:
	return timer

# sets & starts timer
func set_timer(time:float) -> void:
	timer.start(time)

# Connect the timer's timeout signal to a target function
func connect_timer(target:Node, funcname:String) -> void:
# warning-ignore:return_value_discarded
	timer.connect("timeout",target,funcname);










