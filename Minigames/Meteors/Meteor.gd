extends Sprite


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var screenHeight = \
	ProjectSettings.get_setting("display/window/size/height")

# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimationPlayer.playback_speed = rand_range(0.2, 0.8) * (randi()%2*2-1)
	#$AnimationPlayer.play("Spin")

# this actually gets set by the spawner later
var speed = 400

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	self.position.y += speed * delta;
	
	if self.position.y > screenHeight:
		#print("RIP in peace")
		self.queue_free()
