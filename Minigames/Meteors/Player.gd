extends MeshInstance2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


export var speed = 700.0  # speed in pixels/sec
export var acceleration = 80.0 # acceleration in pixels/sec^2
export var friction = 50.0 # decceleration in pixels/sec^2

var velocity = Vector2.ZERO
var inputVector = Vector2.ZERO

func get_input():
	inputVector = Vector2.ZERO
	if Input.is_action_pressed("p1_right"):
		inputVector.x += 1
	if Input.is_action_pressed('p1_left'):
		inputVector.x -= 1
	if Input.is_action_pressed('p1_down'):
		inputVector.y += 1
	if Input.is_action_pressed('p1_up'):
		inputVector.y -= 1
	
	# Make sure diagonal movement isn't faster
	inputVector = inputVector.normalized()

func _process(delta):
	
	get_input()
	
	# Accelerate
	if inputVector.length() > 0:
		velocity = lerp(velocity, inputVector*speed, acceleration/speed)
	
	# Deccelerate
	elif velocity.length() > friction:
		velocity = lerp(velocity, Vector2.ZERO, friction/velocity.length())
	
	# stop
	else:
		velocity = Vector2.ZERO
	
	velocity = velocity.clamped(speed)
	
	# actually move
	#print(velocity*delta)
	self.position += velocity * delta
	
	# update shader
	material.set_shader_param("angle", -velocity.x/speed)


func _on_Collider_area_entered(area):
	print((area))
	if area.is_in_group("Meteors"):
		# You Died
		MGFramework.end_game(false)
		self.queue_free()
