# Game: [Game Name]
# Programming by [programmer]
# [Other Contributers and their respective roles]
extends Node2D

# _ready() is called when the game starts
func _ready():
	# This sets the timer for 4 seconds.
	# Feel free to adjust this to suit your minigame!
	MGFramework.set_timer(12)
	
	# This connects the framework timer to the function _on_Timer_timeout()
	# I recommend you not change this unless you know what you're doing.
	MGFramework.connect_timer(self, "_on_Timer_timeout")
	
	MGFramework.add_points(10)


# This function is linked to the "Button" node in the scene tree
func _on_Button_pressed():
	# End game as a win, and give the player 100 points
	MGFramework.set_score(100)
	MGFramework.end_game(true); 


# This function will be triggered when the game timer runs out
func _on_Timer_timeout():
	# End the game as a loss, and give the player 0 points
	MGFramework.add_points(300)
	MGFramework.end_game(true); 


func _on_ScoreTimer_timeout():
	MGFramework.add_points(10)
