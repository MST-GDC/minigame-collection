extends Timer


const MeteorScene = preload("res://Minigames/Meteors/Meteor.tscn");

const spawnY = -20;
onready var screenWidth = \
	ProjectSettings.get_setting("display/window/size/width")

# Game scene to add meteors and children of
onready var gameScene = $".."

export var spawnTime := 0.75
export var spawnTimeVariation := 0.25
export var spawnQtyMax := 1
export var meteorSpeed := 400.0
export var meteorSpeedVariation := 100.0

# Called when the node enters the scene tree for the first time.
func _ready():
	set_wait_time(spawnTime);
	start()

func spawnMeteor():
	var meteor = MeteorScene.instance()
	# meteor.speed = curSpeed;
	
	# place the metor in a random location
	meteor.position = Vector2(screenWidth * randf(), spawnY)
	
	# set speed
	meteor.speed = meteorSpeed + \
		rand_range(-meteorSpeedVariation, meteorSpeedVariation)
	
	self.add_child(meteor);

func _on_self_timeout():
	for i in range(randi() % spawnQtyMax + 1):
		spawnMeteor();
	set_wait_time(spawnTime + \
		rand_range(-spawnTimeVariation, spawnTimeVariation))
