shader_type canvas_item;

uniform float angle;

void vertex() {
	// VERTEX += cos(TIME) * vec2(15, 20);
	VERTEX.x *= 1.0-abs(0.1*angle);
	VERTEX.y *= 1.0+(0.0015*angle*VERTEX.x);
	// COLOR = vec4(UV, 0.9, 1.0);
}