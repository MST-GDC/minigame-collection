extends Node2D

# Called when the node enters the scene tree for the first time.
func _ready():
	MGFramework.connect_timer(self, "_on_Timer_timeout")
	MGFramework.set_timer(5)


# Small Win 50
func _on_Button_pressed():
	var pos = get_global_mouse_position()
	MGFramework.spawnScorePopup(pos.x, pos.y, 50)
	
	MGFramework.set_score(50)
	MGFramework.end_game(true);

# Big Win 200
func _on_Button2_pressed():
	var pos = get_global_mouse_position()
	MGFramework.spawnScorePopup(pos.x, pos.y, 200)
	
	MGFramework.set_score(200)
	MGFramework.end_game(true);

# Lose
func _on_Button3_pressed():
	MGFramework.end_game(false)

# Lose by Timeout
func _on_Timer_timeout():
	MGFramework.end_game(false)
