extends Area2D


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	for body in get_overlapping_bodies():
		if body is RigidBody2D:
			body.apply_central_impulse($RayCast2D.cast_to*delta*3*get_parent().scale.x)
