extends Node2D

export var STACK_COUNT := 6
export var ITEM_SPACING := 70

var object_resource = load("res://Minigames/Sort/Subscenes/SortObject.tscn")

var queue = []

# Called when the node enters the scene tree for the first time.
func _ready():
	for i in range(STACK_COUNT):
		var item = getNewItem()	# get a new item object
		item.position += Vector2(0, -i*ITEM_SPACING) # position item in chute
		queue.push_back(item); # add item to queue for tracking
		add_child(item);
		

func getNewItem() -> Node2D:
	return object_resource.instance()


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
