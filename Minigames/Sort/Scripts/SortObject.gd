extends RigidBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var green_texture = preload("res://Minigames/Sort/Sprites/Lettuce_Boy_Green.png")
var red_texture = preload("res://Minigames/Sort/Sprites/Lettuce_Boy_Red.png")

# Called when the node enters the scene tree for the first time.
func _ready():
	if randi()%2:
		# green
		$Sprite.texture = green_texture;
		add_to_group("green");
	else:
		$Sprite.texture = red_texture;
		add_to_group("red");


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
