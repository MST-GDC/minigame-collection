# Game: [Game Name]
# Programming by [programmer]
# [Other Contributers and their respective roles]
extends Node2D

onready var SortZone = $SortZone

const SORT_IMPULSE = Vector2(100, -20)

# _ready() is called when the game starts
func _ready():
	# This sets the timer for 4 seconds.
	# Feel free to adjust this to suit your minigame!
	MGFramework.set_timer(50)
	
	# This connects the framework timer to the function _on_Timer_timeout()
	# I recommend you not change this unless you know what you're doing.
	MGFramework.connect_timer(self, "_on_Timer_timeout")


# the _process() function is called every frame. 
# 'delta' is the elapsed time since the previous frame.
# This may be useful for implementing your game code.
func _process(_delta):
	# warning-ignore:return_value_discarded
	if Input.is_action_just_pressed("debug_reset"): 
		MGFramework.set_score(0)
		get_tree().reload_current_scene()

func _input(event):
	var direction = 0
	if event.is_action_pressed("p1_right"):
		direction = 1
	if event.is_action_pressed("p1_left"):
		direction = -1
	if direction != 0 and SortZone.get_overlapping_bodies():
		push_bodies(direction)

func push_bodies(dir : int) -> void:
	for body in SortZone.get_overlapping_bodies():
		body.apply_central_impulse(Vector2(SORT_IMPULSE.x*dir, SORT_IMPULSE.y))
		#print(body.is_in_group("green"))
		

# This function is linked to the "Button" node in the scene tree
func _on_Button_pressed():
	# End game as a win, and give the player 100 points
	MGFramework.set_score(100); 
	MGFramework.end_game(true); 


# This function will be triggered when the game timer runs out
func _on_Timer_timeout():
	MGFramework.end_game(true);



func _on_LeftArea_body_entered(body):
	if body.is_in_group("green"):
		MGFramework.add_points(100);
		MGFramework.spawnScorePopup(body.position.x, body.position.y, 100)
	elif body.is_in_group("red"):
		MGFramework.add_points(-100);
		MGFramework.spawnScorePopup(body.position.x, body.position.y, -100)


func _on_RightArea_body_entered(body):
	if body.is_in_group("red"):
		MGFramework.add_points(100);
		MGFramework.spawnScorePopup(body.position.x, body.position.y, 100)
	elif body.is_in_group("green"):
		MGFramework.add_points(-100);
		MGFramework.spawnScorePopup(body.position.x, body.position.y, -100)

# NOTE: This is probably very very slow
func _on_HopperArea_body_exited(body):
	if $HopperArea.get_overlapping_bodies().size()==1:
		MGFramework.set_timer(3);
