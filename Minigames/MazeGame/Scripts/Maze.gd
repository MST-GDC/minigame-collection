# Game: Physics Maze
# Programming by Athakaspen
extends Node2D

onready var MazeView = $MazeView
onready var Maze = $Viewport/MazeHolder
onready var Gravity = $Viewport/GravityHack
export var rotAcc := 1.2; # rad/sec^2
export var maxRotSpeed := 1.5; # rad/sec
var rotSpeed = 0;

# _ready() is called when the game starts
func _ready():
	# This sets the timer for 4 seconds.
	# Feel free to adjust this to suit your minigame!
	MGFramework.set_timer(20)
	MGFramework.set_score(2000)
	
	# This connects the framework timer to the function _on_Timer_timeout()
	# I recommend you not change this unless you know what you're doing.
	MGFramework.connect_timer(self, "_on_Timer_timeout")


func _process(delta):
	if Input.is_action_pressed("p1_right"):
		rotSpeed += rotAcc*delta;
	elif Input.is_action_pressed("p1_left"):
		rotSpeed -= rotAcc*delta;
	else:
		rotSpeed = move_toward(rotSpeed, 0, rotAcc * 0.4 * delta);
	rotSpeed = clamp(rotSpeed, -maxRotSpeed, maxRotSpeed)
	MazeView.rotation += rotSpeed*delta;
	Gravity.rotation -= rotSpeed*delta;
	

# This function will be triggered when the game timer runs out
func _on_Timer_timeout():
	# End the game as a loss, and give the player 0 points
	MGFramework.set_score(0);
	MGFramework.end_game(false);


func _on_Button_pressed():
	MGFramework.set_score(0);
	MGFramework.end_game(false)


func _on_Area2D_body_exited(body):
	if body.name == "Ball":
		MGFramework.add_points(1000);
		
		var pos = body.position.rotated(MazeView.rotation)*MazeView.scale.x
		pos += $MazeView.position # offset
		MGFramework.spawnScorePopup(pos.x, pos.y, 1000)
		
		MGFramework.end_game(true);

func _on_ScoreTimer_timeout():
	MGFramework.add_points(-10)
