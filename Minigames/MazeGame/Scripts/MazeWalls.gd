extends Node2D

# Called when the node enters the scene tree for the first time.
func _ready():
	randomizeWalls()

# set each wall to a random rotation
func randomizeWalls() -> void:
	for wall in get_children():
		wall.rotation_degrees = 90*floor(rand_range(0,4));
