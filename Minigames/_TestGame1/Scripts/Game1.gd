extends Node2D

# _ready() is called when the game starts
func _ready():
	# This connects the game timer to the function _on_Timer_timeout()
	MGFramework.connect_timer(self, "_on_Timer_timeout")
	
	# This sets the timer for 4 seconds.
	# Feel free to adjust this to suit your minigame!
	MGFramework.set_timer(4)


# the _process() function is called every frame. 
# 'delta' is the elapsed time since the previous frame.
# This may be useful for implementing your game code.
func _process(_delta):
	pass

# This function is linked to the "Button" node in the scene tree
func _on_Button_pressed():
	# End game as a win, and give the player 100 points
	# Also make a cool popup
	var pos = get_global_mouse_position()
	MGFramework.spawnScorePopup(pos.x, pos.y, 100)
	
	MGFramework.set_score(100)
	MGFramework.end_game(true)

# This function will be triggered when the game timer runs out
func _on_Timer_timeout():
	# End the game as a loss, and give the player 0 points
	MGFramework.end_game(false)
