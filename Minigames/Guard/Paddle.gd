extends Node2D

enum dir {N,S,E,W}

var targetAngle = 0
onready var sprites = $Sprites
onready var area = $PaddleArea

# Called when the node enters the scene tree for the first time.
func _ready():
	
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	sprites.rotation = lerp_angle(sprites.rotation, targetAngle, 0.4)

func move_to(direction):
	
	match direction:
		dir.N: 
			targetAngle = 0;
		dir.E:
			targetAngle = PI/2;
		dir.S:
			targetAngle = PI;
		dir.W:
			targetAngle = -PI/2;
	
	area.rotation = targetAngle
