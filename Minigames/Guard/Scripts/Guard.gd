# Game: [Game Name]
# Programming by [programmer]
# [Other Contributers and their respective roles]
extends Node2D

enum dir {N,S,E,W}

var note_res = preload("res://Minigames/Guard/Note.tscn")

onready var paddle = $"Central Wrapper/Paddle"
onready var base = $"Central Wrapper/BaseArea"
onready var center = $"Central Wrapper"
onready var BeatTimer = $BeatTimer
onready var player = $NoteTonePlayer

# _ready() is called when the game starts
func _ready():
	randomize()
	# This sets the timer for 4 seconds.
	# Feel free to adjust this to suit your minigame!
	MGFramework.set_timer(13.5)
	
	# This connects the framework timer to the function _on_Timer_timeout()
	# I recommend you not change this unless you know what you're doing.
	MGFramework.connect_timer(self, "_on_Timer_timeout")


# the _process() function is called every frame. 
# 'delta' is the elapsed time since the previous frame.
# This may be useful for implementing your game code.
func _process(delta):
	pass

func _input(event):
	# Paddle Movement
	if event.is_action_pressed("p1_up"):
		paddle.move_to(dir.N)
	elif event.is_action_pressed("p1_right"):
		paddle.move_to(dir.E)
	elif event.is_action_pressed("p1_down"):
		paddle.move_to(dir.S)
	elif event.is_action_pressed("p1_left"):
		paddle.move_to(dir.W)

# This function is linked to the "Button" node in the scene tree
func _on_Button_pressed():
	# End game as a win, and give the player 100 points
	MGFramework.set_score(100)
	MGFramework.end_game(true); 


func _on_Timer_timeout():
	MGFramework.add_points(500)
	MGFramework.end_game(true);


func spawnNote():
	var note = note_res.instance()
	note.rotation = randi() % 4 * (PI/2)
	center.add_child(note)


func _on_BeatTimer_timeout():
	if MGFramework.timer.time_left > 1.5:
		spawnNote()
		BeatTimer.wait_time *= 0.955

func playTone():
	player.pitch_scale += 0.1
	player.play();
