extends Node2D


onready var gameNode = $"../.."

# Called when the node enters the scene tree for the first time.
#func _ready():
#	pass


func _on_NoteArea_area_entered(area):
	if area.is_in_group("Paddle"):
		gameNode.playTone();
		MGFramework.add_points(50)
		self.queue_free()
	if area.is_in_group("Base"):
		MGFramework.end_game(false)
