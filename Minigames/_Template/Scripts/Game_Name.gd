# Game: [Game Name]
# Programming by [programmer]
# [Other Contributers and their respective roles]
extends Node2D


# _ready() is called when the game starts
func _ready():
	# This sets the timer for 4 seconds.
	# Feel free to adjust this to suit your minigame!
	MGFramework.set_timer(4)
	
	# This connects the framework timer to the function _on_Timer_timeout()
	# I recommend you not change this unless you know what you're doing.
	MGFramework.connect_timer(self, "_on_Timer_timeout")


# the _process() function is called every frame. 
# 'delta' is the elapsed time since the previous frame.
# This may be useful for implementing your game code.
func _process(delta):
	pass


# This function is linked to the "Button" node in the scene tree
func _on_Button_pressed():
	# End game as a win, and give the player 100 points
	MGFramework.end_game(true, 100); 


# This function will be triggered when the game timer runs out
func _on_Timer_timeout():
	# End the game as a loss, and give the player 0 points
	MGFramework.end_game(false, 0);



