# Game: [Keep Up]
# Programming by [Santino Vuocolo]
# [Other Contributers and their respective roles]
extends Node2D
var numBalls = 3
onready var sound = $"Lose Zone/AudioStreamPlayer2D"
onready var currScoreLabel = $"Current Score"
# _ready() is called when the game starts
func _ready():
	# This sets the timer for 4 seconds.
	# Feel free to adjust this to suit your minigame!
	MGFramework.set_timer(15)
	
	# This connects the framework timer to the function _on_Timer_timeout()
	# I recommend you not change this unless you know what you're doing.
	MGFramework.connect_timer(self, "_on_Timer_timeout")
	
	MGFramework.set_score(numBalls * 700);


# the _process() function is called every frame. 
# 'delta' is the elapsed time since the previous frame.
# This may be useful for implementing your game code.
func _process(delta):
	var text = "Current Score: " + str(numBalls * 100)
	currScoreLabel.set_text(text)
	if(numBalls < 1):
		MGFramework.set_score(0);
		MGFramework.end_game(false);


# This function will be triggered when the game timer runs out
func _on_Timer_timeout():
	# End the game
	MGFramework.end_game(true);


func _on_Lose_Zone_body_entered(body):
	body.free()
	sound.play()
	numBalls -= 1
	MGFramework.set_score(numBalls * 600);
