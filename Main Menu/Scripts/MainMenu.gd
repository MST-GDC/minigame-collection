extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Game_1_pressed():
	MGFramework.start_game("Game 1");


func _on_Game_2_pressed():
	MGFramework.start_game("Game 2");


func _on_Sort_pressed():
	MGFramework.start_game("Sort");


func _on_Maze_pressed():
	MGFramework.start_game("Maze");


func _on_Meteors_pressed():
	MGFramework.start_game("Meteors");


func _on_Guard_pressed():
	MGFramework.start_game("Guard");


func _on_Balloon_pressed():
	MGFramework.start_game("Balloon");
