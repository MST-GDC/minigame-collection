<!-- <a href="http://fvcproductions.com"><img src="https://avatars1.githubusercontent.com/u/4284691?v=3&s=200" title="FVCproductions" alt="FVCproductions"></a> -->

<!-- [![FVCproductions](https://avatars1.githubusercontent.com/u/4284691?v=3&s=200)](http://fvcproductions.com) -->

# S&T GDC [ACM-Arcade] Fall 2020 Project: Minigame Collection (working title)

> A game created by the S&T Game Development Club using the Godot engine.

- Status: Beginning development

<!-- [![INSERT YOUR GRAPHIC HERE](http://i.imgur.com/dt8AUb6.png)]() -->

---

## Using MGFramework

> The MGFramework (short for Minigame Framework - creative, I know) handles the backend "metagame" logic. It serves two main purposes:

- Automate and manage the starting of minigames during a playsession
- Provide useful tools and functionality for your minigame

**There are a few functions you should know about before making your minigame.**

- `MGFramework.set_score(score)`  
  - Fairly self-explanatory. `score` is an int representing the value to set the current score to.  
- `MGFramework.end_game(winState)`  
  - This function is used to end your game and pass information back to the Framework.  
  - `winState` is a bool representing whether the player won or lost. `true` represents a win.
- `MGFramework.set_timer(time)`
  - This function should be called once when your game starts. It tells the framework to start a timer for the specified time, representing the duration of your minigame.
  - `time` is a float representing the time in seconds.  
- `func add_points(points)`
  - This one's pretty easy, it just adds the value of `points` to the player's score

### Clone

- Clone this repo to your local machine using `https://gitlab.com/MST-GDC/minigame-collection.git`

## Team

> Or Contributors/People
